#!/usr/bin/env python

# This tells Python to load the Gimp module 
from gimpfu import *
from gimpenums import *

gettext.install("gimp20-python", gimp.locale_directory, unicode=True)

# This is the function that will perform actual actions
def split_layers(image):
    gimp.context_push()
    image.undo_group_start()
    for layer in image.layers:
        if not layer.visible:
            continue # ignore invisible layers
        new_layer = layer.copy()
        pos = image.layers.index(layer)
        image.insert_layer(new_layer, position=pos + 1)
        w, h = layer.width, layer.height
        if w > h:
            w /= 2
            offx, offy = -w, 0
        else:
            h /= 2
            offx, offy = 0, -h
        layer.resize(w, h, 0, 0)
        new_layer.resize(w, h, offx, offy)
        new_layer.set_offsets(0, 0)
    image.undo_group_end()
    gimp.context_pop()


# This is the plugin registration function
# I have written each of its parameters on a different line 
register(
    "split-in-half",    
    N_("Split all layers in half along shortest dimension. "
       "Useful to split pages apart when scanned two at a time."),
    "Split all layers in half along shortest dimension.", 
    "Carl Osterwisch", 
    "Carl Osterwisch", 
    "June 2013",
    label=N_("Split in half"),
    imagetypes="*", 
    params=[
        (PF_IMAGE, "image", "Input image", None),
    ], 
    results=[],
    function=split_layers,
    menu="<Image>/Layer/Stack", 
    domain=("gimp20-python", gimp.locale_directory)
    )

main()

