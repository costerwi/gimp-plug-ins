#!/usr/bin/env python

# This tells Python to load the Gimp module 
from gimpfu import *
from gimpenums import *

gettext.install("gimp20-python", gimp.locale_directory, unicode=True)

# This is the function that will perform actual actions
def layers_hide_above(image):
    gimp.context_push()
    image.undo_group_start()
    v = 0
    for layer in image.layers:
        if not v and image.active_layer == layer:
            v = 1
        layer.visible = v
    image.undo_group_end()
    gimp.context_pop()


# This is the plugin registration function
register(
    "layers-hide-above", 
    N_("Turn visibility of layers above current layer off."),
    "Turn visibility of layers above current layer off.", 
    "Carl Osterwisch", 
    "Carl Osterwisch", 
    "June 2013",
    label=N_("Hide above current"),
    imagetypes="*", 
    params=[
        (PF_IMAGE, "image", "Input image", None),
    ], 
    results=[],
    function=layers_hide_above,
    menu="<Image>/Layer/Stack", 
    domain=("gimp20-python", gimp.locale_directory)
    )

main()

