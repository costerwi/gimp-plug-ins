#!/usr/bin/env python

# This tells Python to load the Gimp module 
from gimpfu import *
from gimpenums import *

gettext.install("gimp20-python", gimp.locale_directory, unicode=True)

# This is the function that will perform actual actions
def layers_levels(image, low, high, gamma):
    gimp.context_push()
    image.undo_group_start()
    for n, layer in enumerate(image.layers):
        if not layer.visible:
            continue
        pdb.gimp_levels(layer, 0, low, high, gamma, 0, 255)
        gimp.progress_update(100*n/len(image.layers))
    image.undo_group_end()
    gimp.context_pop()


# This is the plugin registration function
register(
    "layers-levels", 
    N_("Modifies intensity levels of all visible layers."),
    "Modifies intensity levels of all visible layers.",
    "Carl Osterwisch", 
    "Carl Osterwisch", 
    "January 2014",
    label=N_("Levels..."),
    imagetypes="*", 
    params=[
        (PF_IMAGE,  "image", "Input image", None),
        (PF_INT8,   "low",   "Low-input", 0),
        (PF_INT8,   "high",  "High-input", 255),
        (PF_SLIDER, "gamma", "Gamma correction", 1, (0.1, 10, 0.1)),
    ], 
    results=[],
    function=layers_levels,
    menu="<Image>/Layer/Stack", 
    domain=("gimp20-python", gimp.locale_directory)
    )

main()

