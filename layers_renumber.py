#!/usr/bin/env python

# This tells Python to load the Gimp module 
from gimpfu import *
from gimpenums import *

gettext.install("gimp20-python", gimp.locale_directory, unicode=True)

# This is the function that will perform actual actions
def layers_renumber(image):
    import re
    findNumber = re.compile('(.*?)(\d+)(\D*)$')
    gimp.context_push()
    image.undo_group_start()
    page = 0
    fmt = ''
    for layer in image.layers:
        if not layer.visible:
            continue
        if not fmt:
            m = findNumber.match(layer.name)
            if m:
                page = int(m.group(2))
                fmt = m.group(1) + "%%0%dd"%len(m.group(2)) + m.group(3)
        else:
            page += 1
            layer.name = fmt%page   # rename layer
    image.undo_group_end()
    gimp.context_pop()


# This is the plugin registration function
# I have written each of its parameters on a different line 

register(
    "layers-renumber", 
    N_("Renumber visible layers based on first template found."),
    "Renumber visible layers based on first template found.",
    "Carl Osterwisch", 
    "Carl Osterwisch", 
    "January 2014",
    label=N_("Renumber"),
    imagetypes="*", 
    params=[
        (PF_IMAGE, "image", "Input image", None),
    ], 
    results=[],
    function=layers_renumber,
    menu="<Image>/Layer/Stack", 
    domain=("gimp20-python", gimp.locale_directory)
    )

main()

