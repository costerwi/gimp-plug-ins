#!/usr/bin/env python

# This tells Python to load the Gimp module 
from gimpfu import *
from gimpenums import *

gettext.install("gimp20-python", gimp.locale_directory, unicode=True)

# This is the function that will perform actual actions
def layers_tile_vert(image):
    gimp.context_push()
    image.undo_group_start()
    yoffset = 0
    for layer in image.layers:
        if not layer.visible:
            continue
        layer.set_offsets(0, yoffset)
        yoffset += layer.height
    image.resize(image.width, yoffset, 0, 0)
    image.undo_group_end()
    gimp.context_pop()


# This is the plugin registration function
# I have written each of its parameters on a different line 

register(
    "layers-tile-vert", 
    N_("Tile layers vertically."),
    "Arrange layers in vertical stack.",
    "Carl Osterwisch", 
    "Carl Osterwisch", 
    "October 2014",
    label=N_("Tile vert"),
    imagetypes="*", 
    params=[
        (PF_IMAGE, "image", "Input image", None),
    ], 
    results=[],
    function=layers_tile_vert,
    menu="<Image>/Layer/Stack", 
    domain=("gimp20-python", gimp.locale_directory)
    )

main()

